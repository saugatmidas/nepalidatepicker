package com.freecode.nepalidatepicker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NepaliDatePickerDialog.onDateSetListener {
    private Button btn_date_picker;
    NepaliDatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_date_picker = findViewById(R.id.btn_date_picker);

        btn_date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new NepaliDatePickerDialog(MainActivity.this,"date");
                datePickerDialog.setOnDateSelectedListener(MainActivity.this);
                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onDateSelected(int year, int month, int day, String tag) {
        if (tag.equals("date")) {
            TextView textView = findViewById(R.id.date);
            textView.setText(NepaliConverter.getDateInNepaliFormat(year, month, day));
        }
    }
}
