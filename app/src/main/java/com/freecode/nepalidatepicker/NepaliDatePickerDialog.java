package com.freecode.nepalidatepicker;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import org.jetbrains.annotations.NotNull;

public class NepaliDatePickerDialog {

    private static int PICKED_DAY = 0;
    private static int PICKED_MONTH = 0;
    private static int PICKED_YEAR = 0;
    private static String TAG = "NP_DATE_PICKER";

    static {
        PICKED_YEAR = 2000;
        PICKED_MONTH = 1;
        PICKED_DAY = 1;
    }

    public interface onDateSetListener {
        void onDateSelected(int year,int month, int day, String tag);
    }

    private onDateSetListener dateInterface;

    public void setOnDateSelectedListener(onDateSetListener onDateSetListener) {
        this.dateInterface = onDateSetListener;
    }

    private Activity activity;
    private String tag = "";
    private AlertDialog.Builder builder;
    private NumberPicker year_picker, month_picker, day_picker;

    public NepaliDatePickerDialog(@NotNull Activity activity,@NotNull String tag) {
        this.activity = activity;
        this.tag = tag;
        builder = new AlertDialog.Builder(this.activity);
        View view;
        builder.setView(view = activity.getLayoutInflater().inflate(R.layout.dialog_nepali_date_picker, null));
        year_picker = view.findViewById(R.id.year_picker);
        month_picker = view.findViewById(R.id.month_picker);
        day_picker = view.findViewById(R.id.day_picker);
        initiateDatePickerDialog();
    }

    private void initiateDatePickerDialog() {
        builder.setTitle("मिति छान्नुहोस्");

        builder.setPositiveButton("सेट गर्नुहोस्", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dateInterface.onDateSelected(PICKED_YEAR, PICKED_MONTH, PICKED_DAY, tag);
            }
        });

        builder.setNegativeButton("रद्द गर्नुहोस्", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        setupYearPicker();
        setupMonthPicker();
        fillDaysInPicker();

        year_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                PICKED_YEAR = newVal;
                fillDaysInPicker();
                Log.v(TAG, String.valueOf(PICKED_YEAR));
            }
        });

        month_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                PICKED_MONTH = newVal;
                fillDaysInPicker();
                Log.v(TAG, String.valueOf(PICKED_MONTH));
            }
        });

        day_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                PICKED_DAY = newVal;
                Log.v(TAG, String.valueOf(PICKED_DAY));
            }
        });
    }

    private void setupYearPicker() {
        year_picker.setDisplayedValues(null);
        year_picker.setWrapSelectorWheel(true);
        year_picker.setMinValue(DateData.getMinYear());
        year_picker.setMaxValue(DateData.getMaxYear());
        year_picker.setDisplayedValues(NepaliConverter.generateNepaliNumber(DateData.getMinYear(), DateData.getMaxYear()));
        year_picker.setValue(PICKED_YEAR);
    }

    private void setupMonthPicker() {
        final String[] months = {"बैशाख", "जेठ", "आषाढ", "साउन", "भाद्र", "आश्विन", "कार्तिक", "मंसिर", "पौष", "माघ", "फाल्गुण", "चैत्र"};
        month_picker.setDisplayedValues(null);
        month_picker.setWrapSelectorWheel(true);
        month_picker.setMinValue(1);
        month_picker.setMaxValue(12);
        month_picker.setDisplayedValues(months);
        month_picker.setValue(PICKED_MONTH);
    }

    private void fillDaysInPicker() {
        try {
            int maxDays = DateData.BS_YEARS_ARRAY[PICKED_YEAR - 2000][PICKED_MONTH];
            day_picker.setDisplayedValues(null);
            day_picker.setWrapSelectorWheel(true);
            day_picker.setMinValue(1);
            day_picker.setMaxValue(maxDays);
            day_picker.setDisplayedValues(NepaliConverter.generateNepaliNumber(1, maxDays));
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG, "fillDaysInPicker: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String getDate() {
        String sYear = String.valueOf(year_picker.getValue());

        String sMonth;
        if (PICKED_MONTH > 9)
            sMonth = String.valueOf(PICKED_MONTH);
        else
            sMonth = "0" + String.valueOf(PICKED_MONTH);

        String sDay;
        if (PICKED_DAY > 9)
            sDay = String.valueOf(PICKED_DAY);
        else
            sDay = "0" + String.valueOf(PICKED_DAY);

        return sYear + "/" + sMonth + "/" + sDay;
    }

    public void show() {
        builder.show();
    }
}
