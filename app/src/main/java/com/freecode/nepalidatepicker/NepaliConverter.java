package com.freecode.nepalidatepicker;

import java.util.ArrayList;

public class NepaliConverter {

    public static String getDateInNepaliFormat(int year, int month, int day) {
        String sDay;
        if (day > 9)
            sDay = String.valueOf(day);
        else
            sDay = "0" + String.valueOf(day);

        return "वि.सं. " + convertRomanIntoDevanagari(String.valueOf(year)) + " साल, "
                + DateData.getNepaliMonth(month) +" "+ convertRomanIntoDevanagari(sDay) + " गते";
    }

    public static String[] generateNepaliNumber(int min, int max) {
        ArrayList<String> nepaliNumberList = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            nepaliNumberList.add(convertRomanIntoDevanagari(String.valueOf(i)));
        }
        return nepaliNumberList.toArray(new String[nepaliNumberList.size()]);
    }

    public static String convertRomanIntoDevanagari(String romanNumber) {
        return romanNumber.replace("1", "\u0967").replace("2", "\u0968").replace("3", "\u0969").replace("4", "\u096a").replace("5", "\u096b").replace("6", "\u096c").replace("7", "\u096d").replace("8", "\u096e").replace("9", "\u096f").replace("0", "\u0966");
    }

}
